<?php

namespace Drupal\multistep\Controller;

class StepRouterController implements StepRouterInterface {

  public function getNumberOfSteps() {
    return 3;
  }

  public function getStepClass(int $stepId) {
    $stepClass = '';

    switch ($stepId) {
      case 1:
        $stepClass = '\Drupal\multistep\Form\StepOneForm';
        break;
      case 2:
        $stepClass = '\Drupal\multistep\Form\StepTwoForm';
        break;
      case 3:
        $stepClass = '\Drupal\multistep\Form\StepThreeForm';
        break;
    }
    return new $stepClass();
  }

  public function getStepButtonClass(int $stepId) {
    $stepButtonClass = '';

    switch ($stepId) {
      case 1:
        $stepButtonClass = '\Drupal\multistep\Buttons\StepOneButtons';
        break;
      case 2:
        $stepButtonClass = '\Drupal\multistep\Buttons\StepTwoButtons';
        break;
      case 3:
        $stepButtonClass = '\Drupal\multistep\Buttons\StepThreeButtons';
        break;
    }
    return new $stepButtonClass();
  }

}
