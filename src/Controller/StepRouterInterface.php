<?php

namespace Drupal\multistep\Controller;

interface StepRouterInterface {

  public function getStepClass(int $stepId);

  public function getStepButtonClass(int $stepId);

}
