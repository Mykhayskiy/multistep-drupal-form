<?php

namespace Drupal\multistep\Controller;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class MultistepService {

  use StringTranslationTrait;

  public function getNumberOfSubmissions() {
    return $this->t('There are <b>' . rand(2, 56) . '</b> submissions in our System. Join now!');
  }

  public function drawProgressBar($currentStep, $numberOfSteps) {
    $progress = [];

    for ($i = 1; $i < $numberOfSteps + 1; $i++) {
      if ($i == $currentStep) {
        $progress[$i] = [
          'class' => 'step-in-progress step-' . $i,
        ];
      }
      elseif ($i < $currentStep) {
        $progress[$i] = [
          'class' => 'step-submitted step-' . $i,
        ];
      }
      else {
        $progress[$i] = [
          'class' => 'step-not-started step-' . $i,
        ];
      }
    }
    return $progress;
  }

}
