<?php

namespace Drupal\multistep\Events;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class Events {

  use StringTranslationTrait;

  public function listOfEvents() {
    //Just a fake values
    //Can be changed to any type of source.
    //API or taxonomy, etc.
    return [
      '' => $this->t("Select Event type"),
      1 => $this->t("Anniversary"),
      2 => $this->t("Birthday"),
      3 => $this->t("Wedding"),
    ];
  }

}
