<?php

namespace Drupal\multistep\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\multistep\Controller\MultistepService;
use Drupal\multistep\Controller\StepRouterController;
use Drupal\multistep\Exception\MultistepException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MultistepBaseForm extends FormBase {

  /**
   * @param \Drupal\multistep\Controller\MultistepService
   */
  protected $multistepService;

  /**
   * @param \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\multistep\Exception\MultistepException
   */
  protected $multistepException;

  /**
   * @var int
   */
  protected $currentStep;

  /**
   * @var []
   */
  protected $formValues;

  /**
   * @var \Drupal\multistep\Controller\StepRouterController
   */
  protected StepRouterController $stepController;

  protected $formFieldNames;

  protected $formRequiredFields;

  protected $numberOfSteps;

  public function __construct(MultistepService   $multistepService,
                              MessengerInterface $messenger) {
    $this->multistepService = $multistepService;
    $this->messenger = $messenger;
    $this->multistepException = new MultistepException();
    $this->currentStep = 1;
    $this->stepController = new StepRouterController();
    $this->numberOfSteps = $this->stepController->getNumberOfSteps();
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('multistep.first.service'),
      $container->get('messenger')
    );
  }

  public function getFormId() {
    return "multistep_base_form";
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->messenger->deleteAll();

    $this->currentStep = !empty($form_state->getValue('current_step')) ? $form_state->getValue('current_step') : $this->currentStep;
    $progress = $this->multistepService->drawProgressBar($this->currentStep, $this->numberOfSteps);

    $form['#theme'] = 'multistep';

    //Fake data implemented as a Service - can be extended/changed
    $form['number_of_submissions'] = [
      '#prefix' => '',
      '#suffix' => '',
      '#type' => 'item',
      '#markup' => $this->multistepService->getNumberOfSubmissions(),
    ];

    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'form-wrapper',
      ],
    ];

    $form['wrapper']['progress_bar'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'steps-wrapper',
        'class' => ['steps-wrapper'],
      ],
    ];

    if (!empty($progress)) {
      foreach ($progress as $stepId => $stepItem) {
        $form['wrapper']['progress_bar']['progress_step_' . $stepId] = [
          '#type' => 'item',
          '#markup' => $stepId,
          '#attributes' => [
            'class' => [$stepItem['class']],
          ],
          '#theme' => 'progress',
        ];
      }
    }

    //If it is not a final step - determine the next/prev step and show form
    if ($this->currentStep != 9) {
      //Receive Fields and Buttons for the Form depending on current step
      $fieldsAndButtons = $this->getFieldsAndButtonsForStep($this->currentStep);
      $this->formFieldNames = $fieldsAndButtons['fields_names'];
      $this->formRequiredFields = $fieldsAndButtons['required_fields'];


      if ($fieldsAndButtons['fields']) {
        $form['wrapper'] += $fieldsAndButtons['fields'];
      }

      if ($fieldsAndButtons['buttons']) {
        $form['wrapper']['actions']['#type'] = 'actions';
        foreach ($fieldsAndButtons['buttons'] as $key => $button) {
          $form['wrapper']['actions']['btn_' . $key] = $button;
        }
      }
    }
    $form['#attached']['library'][] = 'multistep/multistep';

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $stepToNavigate = $form_state->getTriggeringElement()['#goto_step'];

    if ($stepToNavigate > $this->currentStep) {
      foreach ($this->formRequiredFields as $fieldName) {
        if (empty($form_state->getValue($fieldName))) {
          $type = 'text';
          if ($fieldName == 'event') {
            $type = 'select';
          }
          $exceptionMessage = $this->multistepException->returnExceptionText($type, $fieldName);
          $form_state->setErrorByName($fieldName, $exceptionMessage);
        }
      }
    }

    if (!$form_state->getErrors()) {
      //Save values of the Step
      $this->setTempFormValues($this->currentStep, $form_state->getValues());

      $form_state->setValue('current_step', $stepToNavigate);
      $form_state->setRebuild(TRUE);
    }
  }

  public function loadStep(array &$form, FormStateInterface $form_state) {
    $this->messenger->deleteAll();

    $response = new AjaxResponse();

    //Remove Form Errors
    $response = $this->removeFormErrors($response);

    if (!$form_state->getErrors()) {
      $response->addCommand(new HtmlCommand('#form-wrapper', $form['wrapper']));
    }
    else {
      //Add Form Errors
      $errorsToProcess = $form_state->getErrors();
      $response = $this->addFormErrors($response, $errorsToProcess);
    }
    return $response;
  }

  public function getFieldsAndButtonsForStep($stepToNavigate) {
    $stepFormClass = $this->stepController->getStepClass($stepToNavigate);
    if (!empty($this->formValues[$stepToNavigate])) {
      $stepFormClass->setFormValues($this->formValues[$stepToNavigate]);
    }

    return [
      'fields' => $stepFormClass->formFields(),
      'fields_names' => $stepFormClass->formFieldNames(),
      'required_fields' => $stepFormClass->requiredFields(),
      'buttons' => $stepFormClass->formButtons(),
    ];
  }

  public function setTempFormValues($formStep, $fields) {
    foreach ($fields as $fieldName => $fieldValue) {
      if (in_array($fieldName, $this->formFieldNames)) {
        $this->formValues[$formStep][$fieldName] = $fieldValue;
      }
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger->deleteAll();

    $response = new AjaxResponse();

    //Remove Form Errors
    $response = $this->removeFormErrors($response);

    if ($form_state->getErrors()) {
      //Add Form Errors
      $errorsToProcess = $form_state->getErrors();
      $response = $this->addFormErrors($response, $errorsToProcess);
    }
    else {
      //@TODO
      //Save FORM DATA
      //For example $this->saveFormData();
      //$this->formValues contains all information from all steps as an array.

      $form['wrapper'] = [
        '#type' => 'item',
        '#markup' => new TranslatableMarkup('<div class="multistep-form-message-wrapper">
        <div class="top-text-row">Thank you!</div>
        <div class="body-row">Your request has been saved.</div>
        <a class="btn btn-primary" href="/">Front page</a></div>'),
      ];

      $response->addCommand(new HtmlCommand('#form-wrapper', $form['wrapper']));
    }

    return $response;
  }

  public function removeFormErrors(&$response) {
    $response->addCommand(new InvokeCommand('.form-item',
      'removeClass', ['error']));
    $response->addCommand(new InvokeCommand('.form-item .multistep-element',
      'removeClass', ['error']));
    $response->addCommand(new RemoveCommand('.multistep-error-message'));
    return $response;
  }

  public function addFormErrors(&$response, $errorsToProcess) {
    foreach ($errorsToProcess as $errorField => $error) {
      $formElement = '.form-item-' . str_replace('_', '-', $errorField) . ' .multistep-element';
      $response->addCommand(
        new AppendCommand('.form-item-' . str_replace('_', '-', $errorField),
          '<div class="multistep-error-message error">' . $error->render() . '</div>'));
      $response->addCommand(new InvokeCommand($formElement,
        'addClass', ['error']));
    }
    return $response;
  }

}
