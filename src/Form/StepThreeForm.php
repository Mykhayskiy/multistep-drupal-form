<?php

namespace Drupal\multistep\Form;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\multistep\Buttons\StepThreeButtons;
use Drupal\multistep\Events\Events;

class StepThreeForm extends BasicForm {

  use StringTranslationTrait;

  private $formStep;

  private $formButtons;

  private $eventsList;

  public function __construct() {
    $this->formStep = $this->formStep();
    $events = new Events();
    $this->eventsList = $events->listOfEvents();
  }

  public function buttonsClass() {
    return new StepThreeButtons();
  }

  public function formStep() {
    return 3;
  }

  public function formFields() {
    $form['event'] = [
      '#type' => 'select',
      '#title' => $this->t('Event'),
      '#options' => $this->eventsList,
      '#default_value' => !empty(parent::formValues()['event']) ? parent::formValues()['event'] : '',
      '#attributes' => [
        'class' => ['multistep-element'],
      ],
      '#autocomplete' => 'off',
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#format' => 'basic_html',
      '#default_value' => !empty(parent::formValues()['message']) ? parent::formValues()['message'] : '',
      '#attributes' => [
        'class' => ['multistep-element'],
      ],
      '#autocomplete' => 'off',
    ];

    return $form;
  }

  public function formButtons() {
    return $this->buttonsClass()->formButtons();
  }

  public function formName() {
    return "stepThreeForm";
  }

  public function formFieldNames() {
    return [
      'event',
      'message',
    ];
  }

  public function requiredFields() {
    return [
      'event',
      'message',
    ];
  }

}
