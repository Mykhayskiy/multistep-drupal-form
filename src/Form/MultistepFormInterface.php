<?php

namespace Drupal\multistep\Form;

interface MultistepFormInterface {

  public function formName();

  public function formFields();

  public function formValues();

  public function formStep();

  public function formButtons();

}
