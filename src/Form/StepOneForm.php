<?php

namespace Drupal\multistep\Form;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\multistep\Buttons\StepOneButtons;

class StepOneForm extends BasicForm {

  use StringTranslationTrait;

  private $formStep;

  private $formButtons;

  public function __construct() {
    $this->formStep = $this->formStep();
  }

  public function buttonsClass() {
    return new StepOneButtons();
  }

  public function formStep() {
    return 1;
  }

  public function formFields() {
    $form['first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Name'),
      '#default_value' => !empty(parent::formValues()['first_name']) ? parent::formValues()['first_name'] : '',
      '#attributes' => [
        'class' => ['multistep-element'],
      ],
      '#autocomplete' => 'off',
    ];

    $form['last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Name'),
      '#default_value' => !empty(parent::formValues()['last_name']) ? parent::formValues()['last_name'] : '',
      '#attributes' => [
        'class' => ['multistep-element'],
      ],
      '#autocomplete' => 'off',
    ];

    return $form;
  }

  public function formButtons() {
    return $this->buttonsClass()->formButtons();
  }

  public function formName() {
    return "stepOneForm";
  }

  public function formFieldNames() {
    return [
      'first_name',
      'last_name',
    ];
  }

  public function requiredFields() {
    return [
      'first_name',
      'last_name',
    ];
  }

}
