<?php

namespace Drupal\multistep\Form;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\multistep\Buttons\StepTwoButtons;

class StepTwoForm extends BasicForm {

  use StringTranslationTrait;

  private $formStep;

  private $formButtons;

  public function __construct() {
    $this->formStep = $this->formStep();
  }

  public function buttonsClass() {
    return new StepTwoButtons();
  }

  public function formStep() {
    return 2;
  }

  public function formFields() {
    $form['text_field_1_f2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text field 1'),
      '#default_value' => !empty(parent::formValues()['text_field_1_f2']) ? parent::formValues()['text_field_1_f2'] : '',
      '#attributes' => [
        'class' => ['multistep-element'],
      ],
      '#autocomplete' => 'off',
    ];

    $form['text_field_2_f2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text field 2'),
      '#default_value' => !empty(parent::formValues()['text_field_2_f2']) ? parent::formValues()['text_field_2_f2'] : '',
      '#attributes' => [
        'class' => ['multistep-element'],
      ],
      '#autocomplete' => 'off',
    ];

    return $form;
  }

  public function formButtons() {
    return $this->buttonsClass()->formButtons();
  }

  public function formName() {
    return "stepTwoForm";
  }

  public function formFieldNames() {
    return [
      'text_field_1_f2',
      'text_field_2_f2',
    ];
  }

  public function requiredFields() {
    return [
      'text_field_1_f2',
      'text_field_2_f2',
    ];
  }

}
