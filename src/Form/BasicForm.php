<?php

namespace Drupal\multistep\Form;

class BasicForm implements MultistepFormInterface {

  private $formValues;

  public function setFormValues($formValues) {
    $this->formValues = $formValues;
  }

  public function formValues() {
    return $this->formValues;
  }

  public function formName() {
  }

  public function formFields() {
  }

  public function formStep() {
  }

  public function formButtons() {
  }

}
