<?php

namespace Drupal\multistep\Buttons;

interface StepsButtonsInterface {

  public function formButtons();

}
