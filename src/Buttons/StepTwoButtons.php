<?php

namespace Drupal\multistep\Buttons;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class StepTwoButtons implements StepsButtonsInterface {

  use StringTranslationTrait;

  public function formButtons() {

    return [
      [
        '#type' => 'submit',
        '#value' => $this->t('Prev'),
        '#goto_step' => 1,
        '#ajax' => [
          'callback' => '::loadStep',
          'wrapper' => 'form-wrapper',
          'effect' => 'fade',
        ],
      ],
      [
        '#type' => 'submit',
        '#value' => $this->t('Next'),
        '#goto_step' => 3,
        '#ajax' => [
          'callback' => '::loadStep',
          'wrapper' => 'form-wrapper',
          'effect' => 'fade',
        ],
      ],
    ];
  }

}
