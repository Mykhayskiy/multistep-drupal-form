<?php

namespace Drupal\multistep\Buttons;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class StepOneButtons implements StepsButtonsInterface {

  use StringTranslationTrait;

  public function formButtons() {

    return [
      [
        '#type' => 'submit',
        '#value' => $this->t('Next'),
        '#goto_step' => 2,
        '#ajax' => [
          'callback' => '::loadStep',
          'wrapper' => 'form-wrapper',
          'effect' => 'fade',
        ],
      ],
    ];
  }

}
