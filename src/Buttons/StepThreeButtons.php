<?php

namespace Drupal\multistep\Buttons;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class StepThreeButtons implements StepsButtonsInterface {

  use StringTranslationTrait;

  public function formButtons() {
    return [
      [
        '#type' => 'submit',
        '#value' => $this->t('Prev'),
        '#goto_step' => 2,
        '#ajax' => [
          'callback' => '::loadStep',
          'wrapper' => 'form-wrapper',
          'effect' => 'fade',
        ],
      ],
      [
        '#type' => 'submit',
        '#value' => $this->t('Complete & Save'),
        '#goto_step' => 9,
        '#ajax' => [
          'callback' => '::submitForm',
          'wrapper' => 'form-wrapper',
          'effect' => 'fade',
        ],
      ],
    ];
  }

}
