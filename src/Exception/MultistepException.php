<?php

namespace Drupal\multistep\Exception;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class MultistepException implements MultistepExceptionInterface {

  /**
   * use DependencySerializationTrait to prevent
   * "LogicException: The database connection is not serializable.
   * This probably means you are serializing an object that
   * has an indirect reference to the database connection."
   *
   * It can happen in multilingual applications
   */
  use DependencySerializationTrait;
  use StringTranslationTrait;

  public function returnExceptionText($fieldType, $fieldName) {
    $fieldName = ucwords(str_replace('_', ' ', $fieldName));
    switch ($fieldType) {
      case 'text':
        $message = $this->t(MultistepExceptionInterface::TEXT_FIELD_REQUIRED_EXCEPTION, ['@fieldName' => $fieldName]);
        break;
      case 'select':
        $message = $this->t(MultistepExceptionInterface::DROPDOWN_FIELD_REQUIRED_EXCEPTION, ['@fieldName' => $fieldName]);
        break;
      default:
        $message = $this->t(MultistepExceptionInterface::DEFAULT_EXCEPTION);
        break;
    }
    return $message;
  }

}
