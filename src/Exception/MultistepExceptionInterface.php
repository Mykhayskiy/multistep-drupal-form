<?php

namespace Drupal\multistep\Exception;

interface MultistepExceptionInterface {

  const TEXT_FIELD_REQUIRED_EXCEPTION = "@fieldName is required";

  const DROPDOWN_FIELD_REQUIRED_EXCEPTION = "@fieldName is required. Please select a value from the dropdown.";

  const DEFAULT_EXCEPTION = "This field is required";

}
